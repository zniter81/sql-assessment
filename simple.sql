DELIMITER $$
DROP PROCEDURE IF EXISTS `sp_mysalaryreport`$$
CREATE PROCEDURE `sp_mysalaryreport`()
BEGIN

SET @rowNumber = 0;

DROP TABLE IF EXISTS seqTemp;
CREATE TEMPORARY TABLE seqTemp(seq integer);

INSERT INTO  seqTemp
SELECT x1.N + x10.N*10 as seq
FROM (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) x1,
     (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) x10;

INSERT INTO gpls_report_trans_otherinfo(
    pid,
    reference_case_pid,
    transaction_id,
    teacher_id,
    student_id,
    salary_month,
    salary_year,
    salary_amount
)
SELECT @rowNumber := @rowNumber + 1 , 
RTC.reference_case_pid,
RTC.transaction_id , 
RTC.teacher_id ,
RTC.student_id ,
MONTH(OI.salaryDate) , 
YEAR(OI.salaryDate) , 
OI.salaryAmount
FROM claim_information AS RTC
JOIN (
SELECT oi.id, oi.reference_transaction_id as refCasePid, seqtable.seq,
CAST(STR_TO_DATE(REPLACE(JSON_EXTRACT(oi.salary_contributions, CONCAT('$[', seqtable.seq, '].date')),'\"',""),'%d/%m/%Y') AS DATE) salaryDate, JSON_EXTRACT(oi.salary_contributions, CONCAT('$[', seqtable.seq, '].amount')) salaryAmount FROM other_information oi
CROSS JOIN seqTemp seqtable
WHERE seqtable.seq < JSON_LENGTH(salary_contributions)
) AS OI ON OI.refCasePid = RTC.reference_case_pid
AND  OI.salaryDate>= DATE_ADD(DATE_SUB(STR_TO_DATE(RTC.start_dt, '%d/%m/%Y'), INTERVAL 3 MONTH),INTERVAL -DAY(DATE_SUB(STR_TO_DATE(RTC.start_dt, '%d/%m/%Y'), INTERVAL 3 MONTH))+1 DAY) AND OI.salaryDate<= LAST_DAY(STR_TO_DATE(RTC.start_dt, '%d/%m/%Y')) WHERE RTC.type = "E"
ORDER BY seq;

END$$
DELIMITER ;